package it.ganv.webapp.advise.user;

import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;

import it.ganv.webapp.advise.core.Controller;
import it.ganv.webapp.advise.core.Repository;

@ManagedBean
//@ApplicationScoped
@RequestScoped
public class SupplierAddUpdateController extends Controller<Long, User> {
	
	private User user;
	private String message = "";
	private Long id = 0L; 
	
	@Inject
	private Repository<Long, User> userRepository;
	
	public void init() {

		id = this.getParameters().containsKey("ID") ? Long.parseLong(getParameters().get("ID")) : 0L;
		
		if (id.equals(0L)) {
			user = new User();
			user.setTypology(user.SUPPLIER);
		}
		else
		{
			user = userRepository.getbyId(id);
		}
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void addUpdateUser() {

		int res = 0;
		user.setTypology(user.SUPPLIER);

		if(user.getId().equals(0L)) {

			res = userRepository.add(user);
			if(res == 0) {	
				message = "Inserimento avvenuto correttamente";		
			}

			if(res == 1) {			
				message = "Fornitore gi� presente in archivio";		
			}

			if(res == -1) {		
				message = "Errore nell'inserimento";		
			}	
		}
		else{

			res = userRepository.update(user);

			if(res == 0) {			
				message = "Modifica avvenuta correttamente";		
			}

			if(res == 1) {				
				message = "Fornitore gi� presente in archivio";		
			}

			if(res == -1) {				
				message = "Errore nella modifica";		
			}
		}
	}
}
