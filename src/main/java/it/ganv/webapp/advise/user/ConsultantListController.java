package it.ganv.webapp.advise.user;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import it.ganv.webapp.advise.core.Controller;
import it.ganv.webapp.advise.core.Repository;
import it.ganv.webapp.advise.login.Login;

@ManagedBean
//@ApplicationScoped
@RequestScoped
public class ConsultantListController extends Controller<Long, User> {
	
	private List<Login> logins;
	
	@Inject
	private Repository<Long, Login> loginRepository;
	
	public void init() {
		logins = new ArrayList<Login>();
		this.list();
	}
	
	public void list() {
		logins = loginRepository.getAll();
	}
	
	public void delete(Long id, Long iduser) {
		
		User user = new User();
		Login login = new Login();
		
		user.setId(iduser);
		login.setId(id);
		
		login.setUser(user);
	
		loginRepository.delete(login);
		
		try {

			FacesContext.getCurrentInstance().getExternalContext().redirect("consulenti.xhtml");

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public List<Login> getLogins() {
		return logins;
	}

	public void setLogins(List<Login> logins) {
		this.logins = logins;
	}
}