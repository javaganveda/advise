package it.ganv.webapp.advise.user;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import it.ganv.webapp.advise.core.Controller;
import it.ganv.webapp.advise.core.Repository;

@ManagedBean
//@ApplicationScoped
@RequestScoped
public class ClientListController extends Controller<Long, User> {
	
	private List<User> users;
	
	@Inject
	private Repository<Long, User> userRepository;
	
	public void init() {
		users = new ArrayList<User>();
		this.list();
	}
	
	public void list() {
		User user = new User();
		users = userRepository.getbyColumn(user.TYPOLOGY, user.CLIENT);
	}
	
	public void delete(Long id) {
		
		User user = new User();
		user.setId(id);
	
		userRepository.delete(user);
		
		try {

			FacesContext.getCurrentInstance().getExternalContext().redirect("clienti.xhtml");

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}
}