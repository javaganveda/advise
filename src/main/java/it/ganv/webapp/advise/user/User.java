package it.ganv.webapp.advise.user;

//import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

import it.ganv.webapp.advise.core.BaseDomain;

public class User extends BaseDomain<Long> {
	
	//  COSTANTI PER RICERCA
	public final String SURCOMPANY = "surcompany";
	public final String CF = "cf";
	public final String PI = "pi";
	public final String CITY = "city";
	public final String TYPOLOGY = "typology";
	
	//  COSTANTI PER INSERIMENTO IN TYPOLOGY
	public final String CLIENT = "cliente";
	public final String SUPPLIER = "fornitore";
	public final String CONSULTANT = "consulente";
	public final String POTENTIALCLIENT = "potenziale cliente";
	public final String POTENTIALSUPPLIER = "potenziale fornitore";

	private static final long serialVersionUID = 1L;
	
	private String name;
	@Size(min = 1, message = "Campo obbligatorio")
	private String surCompany;
	@Size(min = 1, message = "Campo obbligatorio")
	private String cf;
	private String pi;

	@Size(min = 1, message = "Campo obbligatorio")
	private String typology;
	@Size(min = 1, message = "Campo obbligatorio")
	private String address;
	@Size(min = 1, message = "Campo obbligatorio")
	private String city;
	@Size(min = 1, message = "Campo obbligatorio")
	private String phone;
	@Size(min = 1, message = "Campo obbligatorio")
	//@Email(message = "Inserire una mail")
	private String email;
	@Size(min = 1, message = "Campo obbligatorio")
	private String webSite;
	private String curriculum;
	private String valutation;
	@Size(max = 250, message = "La lunghezza non deve superare i 250 caratteri")
	private String note;
	
	public User () {
		
		this.setId(0L);
		name = "";
		surCompany= "";
		cf= "";
		pi= "";
		typology= "";
		address= "";
		city= "";
		phone= "";
		email= "";
		webSite= "";
		curriculum= "";
		valutation= "";
		note= "";
		
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public String getSurCompany() {
		return surCompany;
	}
	
	public void setSurCompany(String surCompany) {
		this.surCompany = surCompany;
	}
	
	public String getCf() {
		return cf;
	}
	
	public void setCf(String cf) {
		this.cf = cf;
	}
	
	public String getPi() {
		return pi;
	}
	
	public void setPi(String pi) {
		this.pi = pi;
	}

	public String getTypology() {
		return typology;
	}
	
	public void setTypology(String typology) {
		this.typology = typology;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getWebSite() {
		return webSite;
	}
	
	public void setWebSite(String webSite) {
		this.webSite = webSite;
	}
	
	public String getCurriculum() {
		return curriculum;
	}
	public void setCurriculum(String curriculum) {
		this.curriculum = curriculum;
	}
	
	public String getNote() {
		return note;
	}
	
	public void setNote(String note) {
		this.note = note;
	}

	public String getValutation() {
		return valutation;
	}

	public void setValutation(String valutation) {
		this.valutation = valutation;
	}
}
