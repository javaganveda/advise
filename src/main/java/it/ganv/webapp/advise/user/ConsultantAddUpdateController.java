package it.ganv.webapp.advise.user;

import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;

import it.ganv.webapp.advise.core.Controller;
import it.ganv.webapp.advise.core.Repository;
import it.ganv.webapp.advise.login.Login;

@ManagedBean
//@ApplicationScoped
@RequestScoped
public class ConsultantAddUpdateController extends Controller<Long, User> {
	
	private Login login;
	private String message = "";
	private Long id = 0L; 
	
	@Inject
	private Repository<Long, Login> loginRepository;
	
	public void init() {

		id = this.getParameters().containsKey("ID") ? Long.parseLong(getParameters().get("ID")) : 0L;
		
		if (id.equals(0L)) {
			login = new Login();
			login.setId(id);
			login.getUser().setId(id);
			login.getUser().setTypology(login.getUser().CONSULTANT);
		}
		else
		{
			login = loginRepository.getbyId(id);
		}
	}
	
	public Login getLogin() {
		return login;
	}

	public void setLogin(Login login) {
		this.login = login;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void addUpdateUser() {

		int res = 0;
		login.getUser().setTypology(login.getUser().CONSULTANT);

		if(login.getId().equals(0L)) {

			res = loginRepository.add(login);
			if(res == 0) {	
				message = "Inserimento avvenuto correttamente";		
			}

			if(res == 1) {			
				message = "Consulente gi� presente in archivio";		
			}

			if(res == -1) {		
				message = "Errore nell'inserimento";		
			}	
		}
		else{

			res = loginRepository.update(login);

			if(res == 0) {			
				message = "Modifica avvenuta correttamente";		
			}

			if(res == 1) {				
				message = "Consulente gi� presente in archivio";		
			}

			if(res == -1) {				
				message = "Errore nella modifica";		
			}
		}
	}
}
