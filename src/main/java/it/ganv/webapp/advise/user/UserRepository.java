package it.ganv.webapp.advise.user;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.Singleton;
import javax.enterprise.context.Dependent;

import it.ganv.webapp.advise.core.DBRepository;
import it.ganv.webapp.advise.core.Repository;

@Dependent
@Singleton
public class UserRepository implements Repository<Long,User> {
		
	DBRepository dbr = new DBRepository();
	
	public int add(User entity) {
		
		User user = entity;

		int res = 0;

		List<User> users = new ArrayList<User>();

		users = this.getbyColumn(user.CF, user.getCf());

		if (users.size() > 0) {
			return 1;
		}
		else {

			users = this.getbyColumn(user.PI, user.getPi());

			if (users.size() > 0) {
				return 1;
			}
			else {

				user.setName(user.getName().replace ("'", "''"));
				user.setSurCompany(user.getSurCompany().replace ("'", "''"));
				user.setCf(user.getCf().replace ("'", "''"));
				user.setPi(user.getPi().replace ("'", "''"));		
				user.setTypology(user.getTypology().replace ("'", "''"));		
				user.setAddress(user.getAddress().replace ("'", "''"));
				user.setCity(user.getCity().replace ("'", "''"));
				user.setPhone(user.getPhone().replace ("'", "''"));
				user.setEmail(user.getEmail().replace ("'", "''"));
				user.setWebSite(user.getWebSite().replace ("'", "''"));
				user.setCurriculum(user.getCurriculum().replace ("'", "''"));
				user.setValutation(user.getValutation().replace ("'", "''"));
				user.setNote(user.getNote().replace ("'", "''"));

				String sql = String.format("INSERT INTO user (name, surcompany, cf, pi, typology,"
						+ " address, city, phone, email, website, curriculum, valutation, note, visibility) VALUES "
						+ "('%s','%s','%s', '%s','%s','%s','%s','%s','%s','%s', '%s','%s','%s', %b)", 
						user.getName(), user.getSurCompany(), user.getCf(), user.getPi(),
						user.getTypology(), user.getAddress(), user.getCity(), user.getPhone(),
						user.getEmail(), user.getWebSite(), user.getCurriculum(), user.getValutation(), user.getNote(), true);

				res = dbr.insert(sql);

				return res; 
			}	
		}			
	}

	public List<User> getAll() {

		List<User> users = new ArrayList <User> ();

		String sql = String.format("SELECT * FROM user as u WHERE u.visibility=true");

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			User user = new User();

			user.setId( (Long) result.get("id"));
			user.setName( (String) result.get("name"));
			user.setSurCompany( (String) result.get("surcompany"));
			user.setCf( (String) result.get("cf"));
			user.setPi( (String) result.get("pi"));		
			user.setTypology( (String) result.get("typology"));		
			user.setAddress( (String) result.get("address"));
			user.setCity( (String) result.get("city"));
			user.setPhone( (String) result.get("phone"));
			user.setEmail( (String) result.get("email"));
			user.setWebSite( (String) result.get("website"));
			user.setCurriculum( (String) result.get("curriculum"));
			user.setValutation( (String) result.get("valutation"));
			user.setNote( (String) result.get("note"));
			user.setVisibility(true);

			users.add(user);
		}
		return users;	
	}

	@Override
	public int update(User entity) {
		
		User user = entity;
		
		int res = 0;

		user.setName(user.getName().replace ("'", "''"));
		user.setSurCompany(user.getSurCompany().replace ("'", "''"));
		user.setCf(user.getCf().replace ("'", "''"));
		user.setPi(user.getPi().replace ("'", "''"));		
		user.setTypology(user.getTypology().replace ("'", "''"));		
		user.setAddress(user.getAddress().replace ("'", "''"));
		user.setCity(user.getCity().replace ("'", "''"));
		user.setPhone(user.getPhone().replace ("'", "''"));
		user.setEmail(user.getEmail().replace ("'", "''"));
		user.setWebSite(user.getWebSite().replace ("'", "''"));
		user.setCurriculum(user.getCurriculum().replace ("'", "''"));
		user.setValutation(user.getValutation().replace ("'", "''"));
		user.setNote(user.getNote().replace ("'", "''"));

		String sql = String.format("UPDATE user u  SET name = '%s', surcompany = '%s', cf = '%s',"
				+ "pi = '%s', typology = '%s', address = '%s', city = '%s',"
				+ "phone = '%s', email = '%s', website = '%s', curriculum = '%s', valutation = '%s', note = '%s',"
				+ "visibility = %b WHERE u.id = %d",
				user.getName(), user.getSurCompany(), user.getCf(), user.getPi(),
				user.getTypology(), user.getAddress(), user.getCity(), user.getPhone(),
				user.getEmail(), user.getWebSite(), user.getCurriculum(), user.getValutation(),
				user.getNote(), user.isVisible(), user.getId());

		res = dbr.update(sql);

		return res;
	}

	@Override
	public User getbyId(Long id) {

		User user = new User();

		String sql = String.format("SELECT * FROM user as u WHERE u.visibility=true AND id = %d", id);

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			user.setId( (Long) result.get("id"));
			user.setName( (String) result.get("name"));
			user.setSurCompany( (String) result.get("surcompany"));
			user.setCf( (String) result.get("cf"));
			user.setPi( (String) result.get("pi"));		
			user.setTypology( (String) result.get("typology"));		
			user.setAddress( (String) result.get("address"));
			user.setCity( (String) result.get("city"));
			user.setPhone( (String) result.get("phone"));
			user.setEmail( (String) result.get("email"));
			user.setWebSite( (String) result.get("website"));
			user.setCurriculum( (String) result.get("curriculum"));
			user.setValutation( (String) result.get("valutation"));
			user.setNote( (String) result.get("note"));
			user.setVisibility(true);

		}
		return user;	
	}

	@Override
	public List<User> getbyColumn(String column, String value) {
			
		List<User> users = new ArrayList <User> ();

		String sql = "SELECT * FROM user as u WHERE  u.visibility=true AND u." + column + " = '" + value + "'";

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			User user = new User();

			user.setId( (Long) result.get("id"));
			user.setName( (String) result.get("name"));
			user.setSurCompany( (String) result.get("surcompany"));
			user.setCf( (String) result.get("cf"));
			user.setPi( (String) result.get("pi"));		
			user.setTypology( (String) result.get("typology"));		
			user.setAddress( (String) result.get("address"));
			user.setCity( (String) result.get("city"));
			user.setPhone( (String) result.get("phone"));
			user.setEmail( (String) result.get("email"));
			user.setWebSite( (String) result.get("website"));
			user.setCurriculum( (String) result.get("curriculum"));
			user.setValutation( (String) result.get("valutation"));
			user.setNote( (String) result.get("note"));
			user.setVisibility(true);

			users.add(user);
		}
		return users;
	}

	@Override
	public int delete(User entity) {
		
		User user = entity;
		
		int res = 0;

		String sql = String.format("UPDATE user u SET u.visibility = false WHERE id = %d", user.getId());

		res = dbr.delete(sql);

		return res;
	}

	@Override
	public List<User> getbyFilter(String condition) {

		List<User> users = new ArrayList <User> ();

		String sql = "SELECT * FROM user as u WHERE  u.visibility=true AND " + condition;
		System.out.println(sql);
		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			User user = new User();

			user.setId( (Long) result.get("id"));
			user.setName( (String) result.get("name"));
			user.setSurCompany( (String) result.get("surcompany"));
			user.setCf( (String) result.get("cf"));
			user.setPi( (String) result.get("pi"));		
			user.setTypology( (String) result.get("typology"));		
			user.setAddress( (String) result.get("address"));
			user.setCity( (String) result.get("city"));
			user.setPhone( (String) result.get("phone"));
			user.setEmail( (String) result.get("email"));
			user.setWebSite( (String) result.get("website"));
			user.setCurriculum( (String) result.get("curriculum"));
			user.setValutation( (String) result.get("valutation"));
			user.setNote( (String) result.get("note"));
			user.setVisibility(true);

			users.add(user);
		}
		return users;
	}
}