package it.ganv.webapp.advise.job;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import it.ganv.webapp.advise.accounting.Accounting;
import it.ganv.webapp.advise.core.BaseDomain;
import it.ganv.webapp.advise.core.DateTime;
import it.ganv.webapp.advise.user.User;


public class Job extends BaseDomain<Long>{

	private static final long serialVersionUID = 1L;

	@NotNull(message = "Campo obbligatorio")
	private DateTime startDate;
	
	@NotNull(message = "Campo obbligatorio")
	private DateTime duration;
	
	@NotNull(message = "Campo obbligatorio")
	private DateTime finalDate;
	
	@NotNull(message = "Campo obbligatorio")
	private User user;
	
	@NotBlank(message = "Campo obbligatorio")
	private String description;
	
	@NotNull(message = "Campo obbligatorio")
	private int progress;
	
	@NotNull(message = "Campo obbligatorio")
	private double amount;
	
	@NotBlank(message = "Campo obbligatorio")
	private String type;
	
	private String valutation;

	private List<Accounting> payments = new ArrayList<Accounting>();

	public Job() {

		startDate.setDBdateTime("0000-00-00 00:00:00");
		duration.setDBdateTime("0000-00-00 00:00:00");
		finalDate.setDBdateTime("0000-00-00 00:00:00");
		user=new User();
		description="";
		progress=0;
		amount=0;
		type="";
		valutation="";
	}

	public List<Accounting> getPayments() {
		return payments;
	}

	public void setPayments(List<Accounting> payments) {
		this.payments = payments;
	}

	public DateTime getStartDate() {
		return startDate;
	}
	public void setStartDate(DateTime startDate) {
		this.startDate = startDate;
	}
	public DateTime getDuration() {
		return duration;
	}
	public void setDuration(DateTime duration) {
		this.duration = duration;
	}
	public DateTime getFinalDate() {
		return finalDate;
	}
	public void setFinalDate(DateTime finalDate) {
		this.finalDate = finalDate;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getProgress() {
		return progress;
	}
	public void setProgress(int progress) {
		this.progress = progress;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	public String getValutation() {
		return valutation;
	}

	public void setValutation(String valutation) {
		this.valutation = valutation;
	}
}