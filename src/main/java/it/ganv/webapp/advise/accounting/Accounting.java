package it.ganv.webapp.advise.accounting;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import it.ganv.webapp.advise.core.BaseDomain;

public class Accounting extends BaseDomain<Long>{

	private static final long serialVersionUID = 1L;

	@NotBlank(message = "Campo obbligatorio")
	private String date;
	
	@NotNull(message = "Campo obbligatorio")
	private Double amount;

	public Accounting() {

		date="";
		amount = 0.0;
	}

	public String getDate() {
		return date;
	}
	
	public void setDate(String date) {
		this.date = date;
	}
	
	public double getAmount() {
		return amount;
	}
	
	public void setAmount(double amount) {
		this.amount = amount;

	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
