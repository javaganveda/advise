package it.ganv.webapp.advise.core;

import java.io.Serializable;
import java.util.List;

public interface Repository<ID extends Serializable, D extends BaseDomain<ID>> {
	public int add(D entity);
	
	public int update(D entity);
	
	public List<D> getAll();
	
	public D getbyId(ID id);
	
	public List<D> getbyColumn(String column, String value);
	
	public List<D> getbyFilter(String condition);
	
	public int delete(D entity); 	
}
