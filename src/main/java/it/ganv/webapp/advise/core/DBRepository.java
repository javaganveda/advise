package it.ganv.webapp.advise.core;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class DBRepository {

	private static String URL;
	private static final String USER = "root";
	private static final String PASSWORD = "test";

	public  DBRepository() {

		String dbAddress  = "";

		try(BufferedReader bufferedReader = new BufferedReader(new FileReader("dbaddress.sys"))) {

			dbAddress  = bufferedReader.readLine();
			
			URL = dbAddress;

		}catch (Exception e) {
			System.out.println(" file Open Error ");
			e.printStackTrace();
			URL = "jdbc:mysql://localhost:3306/advise?serverTimezone=UTC&autoReconnect=true&useSSl=false";
		}
	}

	public int insert(String sql) {

		Statement statement = null;

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {

			statement = connection.createStatement();

			statement.executeUpdate(sql);
		}
		catch (SQLException e) {
			System.out.println(" DataBase Insert Error ");
			e.printStackTrace();
			return -1;
		}
		return 0;
	}
/*
	public ResultSet select(String sql) {

		ResultSet resultSet = null;

		Connection connection = null;
		Statement statement = null;

		try {

			connection = DriverManager.getConnection(URL, USER, PASSWORD);

			statement = connection.createStatement();

		} catch (SQLException e) {
			System.out.println(" DataBase Open Error ");
			e.printStackTrace();
		}

		try{

			resultSet = statement.executeQuery(sql);

		} catch (SQLException e) {
			System.out.println(" DataBase Select Error ");
			e.printStackTrace();
		}

		return resultSet;
	}
*/
	
	public List<Map<String, Object>> select(String sql) {
		List<Map<String, Object>> results = new ArrayList<Map<String, Object>>();

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(sql);

			ResultSetMetaData rsmd = resultSet.getMetaData();
			
			int columnCount = rsmd.getColumnCount();

			while(resultSet.next()) {
				
				Map<String, Object> result = new HashMap<>();

				for (int i = 1; i <= columnCount; i++) {
					
					String columnName = rsmd.getColumnName(i);
					result.put(columnName, resultSet.getObject(columnName));
				}
				
				results.add(result);
			}
		} catch (SQLException e) {
			System.out.println(" DataBase Open Error ");
			e.printStackTrace();
		}
		return results;
	}
	
	public int update(String sql) {

		Statement statement = null;

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {

			statement = connection.createStatement();
			
			statement.executeUpdate(sql);

		} catch (SQLException e) {
			System.out.println(" DataBase Update Error ");
			e.printStackTrace();
			return -1;
		}
		return 0;
	}

	public int delete(String sql) {

		Statement statement = null;

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)){

			statement = connection.createStatement();

			statement.executeUpdate(sql);

		} catch (SQLException e) {
			System.out.println(" DataBase Delete Error ");
			e.printStackTrace();
			return -1;
		}
		return 0;
	}	
}


