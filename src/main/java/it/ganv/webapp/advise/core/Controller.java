package it.ganv.webapp.advise.core;

import java.io.Serializable;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

public abstract class Controller<ID extends Serializable, D extends BaseDomain<ID>> {

	Map<String, String> parameters;
	
	@PostConstruct
	public void postConstruct() {
		parameters = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();

		init();
	}

	public abstract void init(); 
	

	public Map<String, String> getParameters() {
		return parameters;
	}

	public void setParameters(Map<String, String> parameters) {
		this.parameters = parameters;
	}
}




