package it.ganv.webapp.advise.login;

import javax.validation.constraints.Size;

import it.ganv.webapp.advise.core.BaseDomain;
import it.ganv.webapp.advise.user.User;


public class Login extends BaseDomain<Long>{
	
	private static final long serialVersionUID = 1L;

	// ATTRIBBUTI	
	
	@Size(min = 8, message = "La lunghezza deve essere almeno 8 caratteri")
	private String username;
	
	
	@Size(min = 3, message = "La lunghezza deve essere almeno 3 caratteri")
	private String password;
	
	private User user;      // OGGETTO PER RICHIAMARE LA CLASSE "USER"
	
	// METODI
	public Login () {
		
		this.username = "";
		this.password = "";
		user = new User();
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	public User getUser() {
		return user;
	}
	
	public void setUser(User user) {
		this.user = user;
	}
	
	//metodo toString() restituisce una stringa che pu� essere considerata come la "rappresentazione testuale" dell'oggetto su cui � invocato
	@Override
	public String toString() 
	{
		return this.username + " " + this.password + " ";

	}
}