package it.ganv.webapp.advise.login;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.Singleton;
import javax.enterprise.context.Dependent;

import it.ganv.webapp.advise.core.DBRepository;
import it.ganv.webapp.advise.core.Repository;
import it.ganv.webapp.advise.user.User;
import it.ganv.webapp.advise.user.UserRepository;

@Dependent
@Singleton
public class LoginRepository implements Repository<Long, Login> {
	
	private DBRepository dbr = new DBRepository();

	public int add(Login entity) {

		int res = 0;
		
		Login login = entity;

		List<Login> logins = new ArrayList<Login>();
		List<User> users = new ArrayList<User>();
		List<User> users2 = new ArrayList<User>();

		logins = this.getbyColumn("username", login.getUsername());

		if (logins.size() > 0) {
			return 2;
		}
		else {

			UserRepository ur = new UserRepository();

			users = ur.getbyColumn("cf", login.getUser().getCf());
			users2 = ur.getbyColumn("pi", login.getUser().getPi());

			if (users.size() > 0 || users2.size() > 0) {
				return 1;
			}
			else {

				res = res + ur.add(login.getUser());

				users = ur.getbyColumn("cf", login.getUser().getCf());
				users2 = ur.getbyColumn("pi", login.getUser().getPi());
				if (users.size() > 0) {
					login.setUser(users.get(0));
				}
				if (users2.size() > 0) {
					login.setUser(users2.get(0));
				}

				if (res == 0) {

					login.setUsername (login.getUsername().replace ("'", "''"));
					login.setPassword (login.getPassword().replace ("'", "''"));

					String sql = String.format("INSERT INTO Login (username, password, iduser, visibility) VALUES ('%s','%s', %d, true)",
							login.getUsername(), login.getPassword(), login.getUser().getId());

					res = dbr.insert(sql);
				}
				else {

					res = -1;
				}
			}
		}
		return res;
	}

	public List<Login> getAll() {
		
		List<Login> logins = new ArrayList <Login> ();

		String sql = String.format("SELECT * FROM login as l, user as u WHERE l.visibility=true AND l.iduser = u.id");

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {
			
			User user = new User();
			Login login = new Login();
			
			login.setId((Long) result.get("id"));
			login.setUsername((String) result.get("username"));
			login.setPassword((String) result.get("password"));
			login.setVisibility(true);

			user.setId( (Long) result.get("iduser"));
			user.setName( (String) result.get("name"));
			user.setSurCompany( (String) result.get("surcompany"));
			user.setCf( (String) result.get("cf"));
			user.setPi( (String) result.get("pi"));		
			user.setTypology( (String) result.get("typology"));		
			user.setAddress( (String) result.get("address"));
			user.setCity( (String) result.get("city"));
			user.setPhone( (String) result.get("phone"));
			user.setEmail( (String) result.get("email"));
			user.setWebSite( (String) result.get("website"));
			user.setCurriculum( (String) result.get("curriculum"));
			user.setValutation( (String) result.get("valutation"));
			user.setNote( (String) result.get("note"));
			user.setVisibility(true);
			
			login.setUser(user);

			logins.add(login);
		}
		return logins;	
	}

	@Override
	public int update(Login entity) {

		Login login = entity;

		int res = 0;

		login.setUsername(login.getUsername().replace ("'", "''"));
		login.setPassword(login.getPassword().replace ("'", "''"));

		String sql = String.format("UPDATE login l  SET username = '%s', password = '%s', "
				+ "visibility = true WHERE l.id = %d",
				login.getUsername(), login.getPassword(), login.getId());

		res = dbr.update(sql);
		
		UserRepository ur = new UserRepository();

		res = res + ur.update(login.getUser());

		return res;
	}

	@Override
	public Login getbyId(Long id) {
		
		Login login = new Login();

		String sql = String.format("SELECT * FROM login as l, user as u WHERE l.visibility=true AND l.iduser = u.id AND l.id=%d", id);

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {
			
			User user = new User();
	
			login.setId((Long) result.get("id"));
			login.setUsername((String) result.get("username"));
			login.setPassword((String) result.get("password"));
			login.setVisibility(true);

			user.setId( (Long) result.get("iduser"));
			user.setName( (String) result.get("name"));
			user.setSurCompany( (String) result.get("surcompany"));
			user.setCf( (String) result.get("cf"));
			user.setPi( (String) result.get("pi"));		
			user.setTypology( (String) result.get("typology"));		
			user.setAddress( (String) result.get("address"));
			user.setCity( (String) result.get("city"));
			user.setPhone( (String) result.get("phone"));
			user.setEmail( (String) result.get("email"));
			user.setWebSite( (String) result.get("website"));
			user.setCurriculum( (String) result.get("curriculum"));
			user.setValutation( (String) result.get("valutation"));
			user.setNote( (String) result.get("note"));
			user.setVisibility(true);
			
			login.setUser(user);
		}
		return login;	
	}

	@Override
	public List<Login> getbyColumn(String column, String value) {
		//da modificare
		List<Login> logins = new ArrayList <Login> ();

		String sql;

		value = value.replace("'", "''");

		sql = "SELECT * FROM user as u, login as l WHERE  l.iduser = u.id AND l.visibility= true AND  " + 
				column + " = '" + value + "'";

		List<Map<String, Object>> results;

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {

			User user = new User();
			Login login = new Login();

			user.setId( (Long) result.get("iduser"));
			user.setName( (String) result.get("name"));
			user.setSurCompany( (String) result.get("surcompany"));
			user.setCf( (String) result.get("cf"));
			user.setPi( (String) result.get("pi"));		
			user.setTypology( (String) result.get("typology"));		
			user.setAddress( (String) result.get("address"));
			user.setCity( (String) result.get("city"));
			user.setPhone( (String) result.get("phone"));
			user.setEmail( (String) result.get("email"));
			user.setWebSite( (String) result.get("website"));
			user.setCurriculum( (String) result.get("curriculum"));
			user.setValutation( (String) result.get("valutation"));
			user.setNote( (String) result.get("note"));
			user.setVisibility(true);
			
			login.setId((Long) result.get("iduser"));
			login.setUsername((String) result.get("username"));
			login.setPassword ((String) result.get("password"));
			login.setUser(user);
			login.setVisibility(true);	
			
			logins.add(login);
		}
		return logins;
	}

	@Override
	public int delete(Login entity) {
		
		Login login = entity;
		
		int res = 0;

		String sql = String.format("UPDATE login l SET l.visibility = false WHERE id = %d", login.getId());

		res = dbr.delete(sql);
		
		sql = String.format("UPDATE user u SET u.visibility = false WHERE id = %d", login.getUser().getId());
		
		res = res + dbr.delete(sql);

		return res;
	}

	@Override
	public List<Login> getbyFilter(String condition) {
		
		return null;
	}
	
	public Login checkLogin(Login entity) {

		List<Map<String, Object>> results;
		Login login = entity;
		User user = entity.getUser();
		
													//user sbagliato
		user.setTypology("Utente sconosciuto");
		user.setVisibility(false);

													// uno dei due errato
		String sql = ("SELECT *  FROM user as u, login as l WHERE l.visibility = true AND "
				+ "l.iduser = u.id AND l.username ='" + login.getUsername() + "'");

		results = dbr.select(sql);

		for(Map<String, Object> result : results) {
												
												//username giusto pass sbagliata

			user.setName( (String) result.get("name"));
			user.setSurCompany( (String) result.get("surcompany"));
			user.setCf( (String) result.get("cf"));
			user.setPi( (String) result.get("pi"));
			user.setTypology("Password errata");
			user.setAddress( (String) result.get("address"));
			user.setCity( (String) result.get("city"));
			user.setPhone( (String) result.get("phone"));
			user.setEmail( (String) result.get("email"));
			user.setWebSite( (String) result.get("webSite"));
			user.setCurriculum( (String) result.get("curriculum"));
			user.setNote( (String) result.get("note"));

			sql = "SELECT *  FROM user as u, Login as l WHERE l.visibility = true AND l.iduser = u.id"
					+ " AND l.username ='" + login.getUsername() + "' AND l.password ='"
					+ login.getPassword() + "'";

			results = dbr.select(sql);	

			for(Map<String, Object> result2 : results) {

											// user giusto pass giusta
				
				user.setTypology( (String) result2.get("typology"));
				user.setVisibility(true);
			}
		}
		return login;
	}
}
