package it.ganv.webapp.advise.login;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.validation.constraints.Size;

import it.ganv.webapp.advise.core.Controller;
import it.ganv.webapp.advise.core.Repository;
import it.ganv.webapp.advise.user.User;

@ManagedBean
//@ApplicationScoped
@RequestScoped
public class LoginController extends Controller<Long, Login> {

	@Size(min = 1, message = "Campo obbligatorio")
	private String username="";

	@Size(min = 1, message = "Campo obbligatorio")
	private String password="";

	private Login login;

	@Inject
	private Repository<Long, User> userRepository;

	@PostConstruct
	public void init() {
		login = new Login();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Login getLogin() {
		return login;
	}

	public void setLogin(Login login) {
		this.login = login;
	}

	public void checkLogin() {

		Login ilogin = new Login();

		String page = "";

		ilogin.setUsername(username);	
		ilogin.setPassword(password);

		LoginRepository lr = new LoginRepository();

		this.login = lr.checkLogin(ilogin);

		if(this.login.getUser().getTypology().equals("admin")) {
			// Apri la pagina admin

			page = "admin/home.xhtml";
		}
		else {
			if(this.login.getUser().getTypology().equals("consulente")) {
				// Apri la pagina consulente

				page = "user/home.xhtml";
			}	
		}
		
		if(this.login.getUser().getTypology().equals("admin") || this.login.getUser().getTypology().equals("consulente")) {
			try {

				FacesContext.getCurrentInstance().getExternalContext().redirect(page);

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
	}

	public void passwordRecover() {
		// invio di una mail per il recupero della password
	}
}