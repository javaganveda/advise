                                        ADVISE
                 Deriva dal termine inglese advice che significa consulenza.
                 
Gruppo:G.A.N.V

Componenti: Giovanni d'Alise, Antonio Rodolfo Napolitano, Nicola Berardino,Vincenzo Comentale

                                    Breve descrizione:
                                     
Advice � un software per la gestione di lavori commissionati ad una agenzia di consulenza.

                                  Struttura del progetto:
                                  
Il software � progettato su piattaforma web e questo permette all'utente di poterlo utilizzare da qualsiasi terminale senza la necessit� di istallare un softwarwe. Il progetto � diviso in tre moduli: 

1)Il primo modulo � quello anagrafico e permette la gestione dei dati anagrafici sia dei clienti che dei fornitori, � previsto anche una gestione dei consulenti che collaborano con l'azienda stessa, ai quali mediante il rilascio dei credenziali � permesso l'accesso all'utilizzo dell'applicativo.

2)Il secondo modulo riguarda la gestione delle commesse, mediante questo modulo � possibbile visualizzare l'elenco delle commesse in atto, il cliente che le ha commissionate, lo stato di avanzamento dei lavori e la relativa scheda contabile dei pagamenti.

3)Il terzo modulo consente in modo veloce di ricercare  i contatti tra i quali: telefono, e-mail, sito web, di tutti gli utenti inseriti nel nostro elenco
